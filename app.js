// Imports
const express = require('express')
const app = express()
const port = 3005


// Static Files
app.use(express.static('public'))

// Set Views
app.set('views', './views')
app.set('view engine', 'ejs')

app.get('', (req, res) => {
    res.render('index', { text: 'This is EJS'})
})

app.get('/about', (req, res) => {
    res.render('about', { text: 'About Page'})
})



//  Listen on port 3000
app.listen(port, () => console.info(`Listening on port ${port}`))